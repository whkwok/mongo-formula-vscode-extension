# Mongo Formula VSCode Extension

Provide syntax highlight and utilities for [Mongo Formula](https://gitlab.com/whkwok/mongo-formula).

## Features

- Syntax highlight for Mongo Formula in JavaScript and TypeScript string.

- Command to insert Mongo Formula with syntax highlight.

- Command to insert Mongo Expression/Pipeline written in Mongo Formula.

## Usages

To enable syntax highlight, you can prepend `/* mongo-formula */` before string.

You can execute the `insertFormula` and `insertExpressionOrPipeline` commands from the command palette. (`Ctrl+Shift+P`)

![Demo](https://gitlab.com/whkwok/mongo-formula-vscode-extension/-/raw/master/images/demo.gif)

## Settings

### mongo-formula.mongoFormulaPath

A custom path to the mongo-formula module. The default path is `${workspaceFolder}/node_modules/mongo-formula`. If no module is found, the bundled module will to used.

## Parser Configurations

The extension will find the config file under the workspace folder on activation.

The filename of config file is either `.mongoformularc.json` or `.mongoformula.config.json`.

The following properties should be defined in the config file.

- `schemas` - Dictionary of schemas to be used in the parser.
- `options` - Options to be used in the parser.

An example of config file.

```json
{
  "schemas": {
    "user": {
      "name": { "type": "string" },
      "email": { "type": "string" },
      "birthdate": { "type": "date" }
    },
    "pet": {
      "name": { "type": "string" },
      "type": { "type": "string" }
    }
  },
  "options": { "undefinedVarTyp": { "type": "any" } }
}
```

## Release Notes

Refer to [CHANGELOG.md](https://gitlab.com/whkwok/mongo-formula-vscode-extension/-/blob/master/CHANGELOG.md)
