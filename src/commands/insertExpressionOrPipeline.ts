import { commands, Disposable, TextEditor, window } from 'vscode';
import { ConfigService } from '../services/ConfigService';
import { loadMongoFormulaModule } from '../utils';

const commandId = 'mongo-formula.insertExpressionOrPipeline';

function createInsertExpressionOrPipeline(configService: ConfigService) {
  const {
    MongoFormulaParser,
    allFuncDict,
    allStageDict,
  } = loadMongoFormulaModule();
  const parser = new MongoFormulaParser({}, allFuncDict, allStageDict);
  return async function insertExpressionOrPipeline(editor: TextEditor) {
    const { schemas, options } = configService.get();
    const schemaKeys = [...Object.keys(schemas), 'No schema'];
    const schemaKey = await window.showQuickPick(schemaKeys, {
      placeHolder: 'Select the schema',
    });
    if (schemaKey === undefined) return;
    const schema = schemas[schemaKey] ?? {};
    const formulaString = await window.showInputBox({
      placeHolder: 'Input the formula',
    });
    if (formulaString === undefined) return;
    try {
      const parsed = parser.parse(
        formulaString,
        schema,
        undefined,
        undefined,
        options
      );
      editor.edit((editBuilder) => {
        editor.selections.forEach((selection) => {
          editBuilder.replace(selection, JSON.stringify(parsed.val));
        });
      });
    } catch (error) {
      window.showErrorMessage(error.message);
    }
  };
}

export function createInsertExpressionOrPipelineCommand(
  configService: ConfigService
): Disposable {
  return commands.registerTextEditorCommand(
    commandId,
    createInsertExpressionOrPipeline(configService)
  );
}
