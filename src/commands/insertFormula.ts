import { commands, Disposable, SnippetString, TextEditor } from 'vscode';

const commandId = 'mongo-formula.insertFormula';

// trim quotes of selected text if exists
const formulaSnippet = new SnippetString(
  '/* mongo-formula */ `${0:${TM_SELECTED_TEXT/(?:^\'(.*)\'$)|(?:^"(.*)"$)|(?:^`(.*)`$)/${1}${2}${3}/}}`'
);
function insertFormula(editor: TextEditor) {
  editor.selections.forEach((selection) => {
    editor.insertSnippet(formulaSnippet, selection);
  });
}

export function createInsertFormulaCommand(): Disposable {
  return commands.registerTextEditorCommand(commandId, insertFormula);
}
