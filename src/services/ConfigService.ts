import type { ParserOptions, Schema } from 'mongo-formula';
import { readFileSync } from 'fs';
import { Disposable, ExtensionContext, workspace } from 'vscode';

interface MongoFormulaExtensionConfig {
  schemas: { [key: string]: Schema };
  options?: Partial<ParserOptions>;
}
const configPaths = ['.mongoformularc.json', '.mongoformula.config.json'];
const configPathsPattern = `**/{${configPaths.join(',')}}`;

const configStateKey = 'mongo-formula.config';

const defaultConfig: MongoFormulaExtensionConfig = {
  schemas: {},
};

async function _findConfigPath(): Promise<string | undefined> {
  return (
    await workspace.findFiles(configPathsPattern, '**/node_modules/*', 1)
  )[0]?.fsPath;
}

function _getConfig(fsPath: string | undefined): MongoFormulaExtensionConfig {
  if (fsPath) return JSON.parse(readFileSync(fsPath, 'utf8'));
  return defaultConfig;
}

export class ConfigService implements Disposable {
  private disposables: Disposable[] = [];
  constructor(private readonly context: ExtensionContext) {}

  async init(): Promise<this> {
    const workspaceState = this.context.workspaceState;
    let configPath: string | undefined;
    configPath = (
      await workspace.findFiles(configPathsPattern, '**/node_modules/*', 1)
    )[0]?.fsPath;
    await workspaceState.update(configStateKey, _getConfig(configPath));

    const watcher = workspace.createFileSystemWatcher(configPathsPattern);
    watcher.onDidChange((e) => {
      if (configPath === e.fsPath) {
        const config = _getConfig(e.fsPath);
        workspaceState.update(configStateKey, config);
      }
    });
    watcher.onDidCreate((e) => {
      if (configPath === undefined) {
        configPath = e.fsPath;
        const config = _getConfig(configPath);
        workspaceState.update(configStateKey, config);
      }
    });
    watcher.onDidDelete(async (e) => {
      if (configPath === e.fsPath) {
        configPath = await _findConfigPath();
        const config = _getConfig(configPath);
        workspaceState.update(configStateKey, config);
      }
    });
    this.disposables.push(watcher);
    return this;
  }

  get(): MongoFormulaExtensionConfig {
    return this.context.workspaceState.get(configStateKey) ?? defaultConfig;
  }

  dispose(): void {
    this.disposables.forEach((d) => d.dispose());
  }
}
