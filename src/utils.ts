import { existsSync } from 'fs';
import { normalize } from 'path';
import { workspace } from 'vscode';
import type * as _mongoFormula from 'mongo-formula';

export type mongoFormula = typeof _mongoFormula;

const _require =
  typeof __webpack_require__ === 'function' ? __non_webpack_require__ : require;

function getMongoFormulaPath(): string | undefined {
  const path = workspace
    .getConfiguration('mongo-formula')
    .get<string>('mongoFormulaPath')
    ?.replace(
      '${workspaceFolder}',
      workspace.workspaceFolders?.[0]?.uri.fsPath ?? ''
    );
  if (path) return normalize(path);
}

export function loadMongoFormulaModule(): mongoFormula {
  const mongoFormulaPath = getMongoFormulaPath();
  if (mongoFormulaPath && existsSync(mongoFormulaPath))
    return _require(`${mongoFormulaPath}`);
  return require('mongo-formula');
}
