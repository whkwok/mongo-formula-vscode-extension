import { ExtensionContext } from 'vscode';
import { ConfigService } from './services/ConfigService';
import { createInsertFormulaCommand } from './commands/insertFormula';
import { createInsertExpressionOrPipelineCommand } from './commands/insertExpressionOrPipeline';

export async function activate(context: ExtensionContext) {
  const subscriptions = context.subscriptions;
  const configService = await new ConfigService(context).init();
  const insertFormulaCommand = createInsertFormulaCommand();
  const insertExpressionOrPipelineCommand = createInsertExpressionOrPipelineCommand(
    configService
  );
  subscriptions.push(configService);
  subscriptions.push(insertFormulaCommand);
  subscriptions.push(insertExpressionOrPipelineCommand);
}

export function deactivate() {}
